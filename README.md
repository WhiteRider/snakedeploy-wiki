# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Snake Deploy Wiki ###

This repo aims to be the public wiki of a private project of mine, Snake Deploy are a deploy console with automatized command line profile, the goal is to create build and deploy pipelines using command line and if 
any command you need don't exist you can create whenever you want by leveraging the power of python.

#### What is inside ####

By the time of writing the following features are developed:
- Deploy using FTP
- Comparision of build directory vs deployed version (copy only files that have been altered)
- Automatic profile, save all the needed commands for a deploy and execute then later
- Pack into pipi package
- Built-in FTP server (need improvement)